import NavigationFactory from '@fantassin/menu';

const navEls = new NavigationFactory( {
	triggerAttribute: 'data-trigger-menu',
	isOpenClassname: 'is-open',
	htmlScrollLockClassname: 'is-lock',
} );
navEls.init();


// () => {
//   console.log('Hello world!')
// }
