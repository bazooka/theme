<?php

use Timber\Timber;

/**
 * The template for displaying 404 pages (Not Found)
 *
 * Methods for TimberHelper can be found in the /functions sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

// If we want a static 404 page
$context = Timber::context();
Timber::render( '404.twig', $context );

// If we want a dynamic 404 page by page title
/* @TODO: Find a better solution Ask to Flo
$page = get_page_by_title('404');
$post_id = $page->ID;
$post = new Post( $post_id );
$context['post'] = $post;
Timber::render( 'page.twig', $context );
*/
