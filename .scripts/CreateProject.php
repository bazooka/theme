<?php
namespace Bazooka;

require_once('RenameFiles.php');

class CreateProject {
    public static $WordPressInstalledFromComposer = false;
    public static $ThemeFolder = null;
    public static $ThemeName = null;
    public static $root = null;

    public static function init() {
        echo "\n\n";
        echo "🚀 Welcome to Bazooka! \n\n";

        CreateProject::$root = dirname( dirname( __FILE__ ) );
        CreateProject::WordPressInitializer();
        MergeDependencies::init();
        CleanTheme::init();
        CreateProject::RenameFiles();
    }

    public static function WordPressInitializer() {
       /* if ( !CreateProject::IsWordPressInstalled() ) { */
            CreateProject::InstallWordPress();
      /* } */

        CreateProject::InstallWordPressTheme();
    }

    public static function IsWordPressInstalled() {
        $isWordPressInstalled = is_dir('./wp-content');

        if (!$isWordPressInstalled) {
            $isWordPressInstalled = file_exists('.env'); // We are checking if WordPress exist throught BZK Core
        }

        return $isWordPressInstalled;
    }

    public static function InstallWordPress() {
        echo "🎏 WordPress isn't installed, I'm installing it! \n\n";
        echo system("composer create-project fantassin/wordpress wp_tmp");

        CreateProject::$WordPressInstalledFromComposer = true;
    }

    public static function InstallWordPressTheme() {
        /*CreateProject::GetWordPressContentDirectory();*/
        CreateProject::MoveTheme();
        CreateProject::MoveWordPress();
        CreateProject::MoveThemeToWordPress();
    }

    /*public static function GetWordPressContentDirectory() {
        $WPConfigPath = CreateProject::$root . '/wp_tmp/wp-config.php';

        if (CreateProject::$WordPressInstalledFromComposer) {
           $WPConfigPath = CreateProject::$root . '/wp_tmp/wordpress/wp-config.php';
        }

        //require_once($WPConfigPath); Disabled for now, check comments in GitLab issue #14
    }*/

    public static function MoveTheme() {
        $themeName = basename( CreateProject::$root );
        $themeFolder = CreateProject::$root . DIRECTORY_SEPARATOR . $themeName;
        mkdir($themeFolder);

        $files = glob('*', GLOB_BRACE);

        foreach($files as $file){
            // Except WordPress Folder.
            if ($file == 'wp_tmp' || $file == $themeName) {
                continue;
            }
            // Move file.
            rename($file, $themeFolder . DIRECTORY_SEPARATOR . basename($file));
        }
    }

    public static function MoveWordPress() {
        $parentFolder = dirname(dirname(__FILE__));
        $files = array_merge(glob('wp_tmp/*'), glob('wp_tmp/.env'));
        $files = array_merge($files, glob('wp_tmp/.gitignore'));
        $files = array_merge($files, glob('wp_tmp/.htaccess'));

        foreach( $files as $file ){
            rename($file, CreateProject::$root . DIRECTORY_SEPARATOR . basename($file)); // move file
        }

        ResourceManager::removeFolder($parentFolder . DIRECTORY_SEPARATOR . 'wp_tmp');
    }

    public static function MoveThemeToWordPress() {
        echo "What is the name of the theme❓ \n";
        CreateProject::$ThemeName = readline();

        $parentFolder = dirname(dirname(__FILE__));
        CreateProject::$ThemeFolder = $parentFolder . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'themes' . DIRECTORY_SEPARATOR . CreateProject::$ThemeName;

        rename($parentFolder . DIRECTORY_SEPARATOR . basename( CreateProject::$root ), CreateProject::$ThemeFolder);
    }

    public static function RenameFiles() {
        $rename = new RenameFiles();
        $srcFolder = dirname( __DIR__ ) . DIRECTORY_SEPARATOR . 'src';
        $composerFile = dirname( __DIR__ ) . DIRECTORY_SEPARATOR . 'composer.json';

        $rename->replaceStringInFiles(CreateProject::$ThemeFolder, 'theme_name', CreateProject::Normalize(CreateProject::$ThemeName, '_'));
        $rename->replaceStringInFiles(CreateProject::$ThemeFolder, 'theme-name', CreateProject::Normalize(CreateProject::$ThemeName, '-'));
        $rename->replaceStringInFiles(CreateProject::$ThemeFolder, 'Theme name', ucfirst(CreateProject::Normalize(CreateProject::$ThemeName, ' ')));
        $rename->replaceStringInFiles(CreateProject::$ThemeFolder, 'ThemeName', ucfirst(CreateProject::Normalize(CreateProject::$ThemeName, '')));

        $rename->replaceStringInFiles($srcFolder, 'theme_name', CreateProject::Normalize(CreateProject::$ThemeName, '_'));
        $rename->replaceStringInFiles($srcFolder, 'theme-name', CreateProject::Normalize(CreateProject::$ThemeName, '-'));
        $rename->replaceStringInFiles($srcFolder, 'Theme name', ucfirst(CreateProject::Normalize(CreateProject::$ThemeName, ' ')));
        $rename->replaceStringInFiles($srcFolder, 'ThemeName', ucfirst(CreateProject::Normalize(CreateProject::$ThemeName, '')));

        $rename->replaceStringInFile($composerFile, 'ThemeName', ucfirst(CreateProject::Normalize(CreateProject::$ThemeName, '')));
    }

    public static function Normalize($string, $separator) {
        $string = str_replace('\'', '', $string);
        $string = str_replace('"', '', $string);
        $string = str_replace('\^', '', $string);
        $string = str_replace(' ', $separator, $string);
        $string = strtolower($string);
        return $string;
    }
}
