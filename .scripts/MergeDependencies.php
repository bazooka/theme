<?php
namespace Bazooka;


class MergeDependencies {
  public static $root = null;
  public static $themeFolder = null;
  public static $WPComposer = null;
  public static $themeComposer = null;
  public static $WPPackage = null;
  public static $themePackage = null;

  public static function init() {
    MergeDependencies::$root = CreateProject::$root . DIRECTORY_SEPARATOR;
    MergeDependencies::$themeFolder = CreateProject::$ThemeFolder . DIRECTORY_SEPARATOR;

    MergeDependencies::$WPComposer = MergeDependencies::$root . 'composer.json';
    MergeDependencies::$themeComposer = MergeDependencies::$themeFolder . 'composer.json';

    MergeDependencies::$WPPackage = MergeDependencies::$root . 'package.json';
    MergeDependencies::$themePackage = MergeDependencies::$themeFolder . 'package.json';

    /* Check if composer exists */
    if (file_exists(MergeDependencies::$themeComposer)) {
      if (file_exists(MergeDependencies::$WPComposer)) {
        MergeDependencies::MergeComposer();
      } else {
        rename(MergeDependencies::$themeComposer, MergeDependencies::$root);
      }
    }

    /* Check if package exists */
    if (file_exists(MergeDependencies::$themePackage)) {
      if (file_exists(MergeDependencies::$WPPackage)) {
          MergeDependencies::MergePackage();
      } else {
          rename(MergeDependencies::$themePackage, MergeDependencies::$WPPackage);
      }
    }
  }

  public static function MergeComposer() {
    $composerThemeJSON = MergeDependencies::GetArrayFromJSONPath(MergeDependencies::$themeComposer);
    $composerWordPressJSON = MergeDependencies::GetArrayFromJSONPath(MergeDependencies::$WPComposer);

    if ( array_key_exists('require', $composerThemeJSON) && array_key_exists('require', $composerWordPressJSON)) {
      $composerThemeJSON['require'] = array_merge(
        $composerWordPressJSON['require'],
        $composerThemeJSON['require']
      );
    }

    if ( array_key_exists('require-dev', $composerThemeJSON) && array_key_exists('require-dev', $composerWordPressJSON)) {
      $composerThemeJSON['require-dev'] = array_merge(
        $composerWordPressJSON['require-dev'],
        $composerThemeJSON['require-dev']
      );
    }

    unset($composerThemeJSON['autoload']['psr-4']['Bazooka\\']);

    $composerNewFile = json_encode($composerThemeJSON, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
    file_put_contents(MergeDependencies::$WPComposer, $composerNewFile);
  }

  public static function MergePackage() {
    $packageThemeJSON = MergeDependencies::GetArrayFromJSONPath(MergeDependencies::$themePackage);
    $packageWordPressJSON = MergeDependencies::GetArrayFromJSONPath(MergeDependencies::$WPPackage);

    if ( array_key_exists('dependencies', $packageThemeJSON) && array_key_exists( 'dependencies', $packageWordPressJSON)) {
        $packageWordPressJSON['dependencies'] = array_merge( $packageThemeJSON['dependencies'], $packageWordPressJSON['dependencies'] );
    }

    if ( array_key_exists('devDependencies', $packageThemeJSON) && array_key_exists( 'devDependencies', $packageWordPressJSON)) {
      $packageWordPressJSON['devDependencies'] = array_merge(
          $packageThemeJSON['devDependencies'],
          $packageWordPressJSON['devDependencies']
      );
    }

    $packageNewFile = json_encode($packageThemeJSON, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
    file_put_contents(MergeDependencies::$themePackage, $packageNewFile);
  }

  public static function GetArrayFromJSONPath ( $path ) {
    $fileContent = file_get_contents($path);
    return json_decode($fileContent, true);
  }
}
