<?php

namespace Bazooka;

class CleanTheme
{
    public static function init()
    {
        self::cleanAutoloader();

        ResourcesManager::move(
            ['src', '.gitlab-ci.yml', 'php_cs.xml'],
            MergeDependencies::$themeFolder,
            MergeDependencies::$root
        );

        ResourcesManager::delete(
            [
                MergeDependencies::$themeFolder . 'vendor',
                MergeDependencies::$themeFolder . 'composer.json',
                MergeDependencies::$themeFolder . 'composer.lock',
                MergeDependencies::$themeFolder . 'package.json',
                MergeDependencies::$themeFolder . 'package-lock.json'
            ]
        );
    }

    public static function cleanAutoloader()
    {
        $functionsFile = MergeDependencies::$themeFolder . 'functions.php';

        if (file_exists($functionsFile)) {
            $file = file($functionsFile);
            array_splice($file, 9, 23); // Remove the specific parts of vendor autoload
            file_put_contents($functionsFile, $file);
        }
    }
}