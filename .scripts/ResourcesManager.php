<?php

namespace Bazooka;

class ResourcesManager
{
    /**
     * Delete multiple resources
     *
     * @param array $resources
     */
    public static function delete(array $resources)
    {
        foreach ($resources as $resourcePath) {
            ResourceManager::remove($resourcePath);
        }
    }

    /**
     * Move resources from an src to a destination.
     *
     * @param array $resources
     * @param string $srcPath
     * @param string $destPath
     */
    public static function move(array $resources, string $srcPath, string $destPath)
    {
        foreach ($resources as $resource) {
            ResourceManager::move($srcPath . $resource, $destPath . $resource);
        }
    }
}