<?php

namespace Bazooka;

class ResourceManager
{
    /**
     * Move a resource (file or folder) to a destination path.
     *
     * @param string $srcPath
     * @param string $destPath
     */
    public static function move(string $srcPath, string $destPath)
    {
        if (file_exists($srcPath)) {
            rename($srcPath, $destPath);
        }
    }

    /**
     * @param string $resourcePath
     *
     * @return bool
     */
    public static function remove(string $resourcePath): bool
    {
        if (self::removeFile($resourcePath)) {
            return true;
        }

        if (self::removeFolder($resourcePath)) {
            return true;
        }

        return false;
    }

    /**
     * Remove if the resource is a file.
     *
     * @param string $filePath
     *
     * @return bool
     */
    public static function removeFile(string $filePath): bool
    {
        if (file_exists($filePath) && is_file($filePath)) {
            return unlink($filePath);
        }

        return false;
    }

    /**
     * Remove if the resource is a folder.
     *
     * @param string $folderPath
     *
     * @return bool
     */
    public static function removeFolder(string $folderPath): bool
    {
        if ( ! file_exists($folderPath) && ! is_dir($folderPath)) {
            return false;
        }

        foreach (scandir($folderPath) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if ( ! self::remove($folderPath . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }
        }

        if (is_dir($folderPath)) {
            return rmdir($folderPath);
        }
    }
}