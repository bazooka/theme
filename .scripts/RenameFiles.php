<?php

namespace Bazooka;

class RenameFiles {

  public function replaceStringInFiles( $dir, $search, $replace, $function_exec = '' ) {
    $files = array_diff( scandir( $dir ), array( '.', '..' ) );

    $key = array_search('.gitignore', $files);

    if ( $key !== false ) {
      $excludedFolders = $this->parse_git_ignore_file('.gitignore');
      array_push($excludedFolders, '.scripts');

      foreach ($excludedFolders as $deletedValue) {
        if (($key = array_search($deletedValue, $files)) !== false) {
          unset($files[$key]);
        }
      }
    }

    foreach ( $files as $file ) {
      if ( is_dir( "$dir/$file" ) ) {
        $this->replaceStringInFiles( "$dir/$file", $search, $replace, $function_exec );
      } else {
        $fileModification = @file_get_contents( "$dir/$file" );
        if ( $fileModification !== false ) {
          if ( ! empty( $function_exec ) ) {
            $newFile = str_replace( $search, call_user_func( $function_exec, $replace ), $fileModification );
          } else {
            $newFile = str_replace( $search, $replace, $fileModification );
          }

          file_put_contents( "$dir/$file", $newFile );
        }
      }
    }
  }

  public function replaceStringInFile( $dirfile, $search, $replace, $function_exec = '' ) {
    $fileModification = @file_get_contents( "$dirfile" );
    if ( $fileModification !== false ) {
      // Callback
      if ( ! empty( $function_exec ) ) {
        $newFile = str_replace( $search, call_user_func( $function_exec, $replace ), $fileModification );
      } else {
        $newFile = str_replace( $search, $replace, $fileModification );
      }

      return file_put_contents( "$dirfile", $newFile );
    }
  }

  public function renameFiles( $dir, $search, $replace, $function_exec = '' ) {
    $files = array_diff( scandir( $dir ), array( '.', '..' ) );

    foreach ( $files as $file ) {
      if ( is_dir( "$dir/$file" ) ) {
        $this->renameFiles( "$dir/$file", $search, $replace, $function_exec );
      } else {
        $position = stripos( "$dir/$file", $search );

        if ( $position !== false ) {
          if ( ! empty( $function_exec ) ) {
            $newFile = str_replace( $search, call_user_func( $function_exec, $replace ), "$dir/$file" );
          } else {
            $newFile = str_replace( $search, $replace, "$dir/$file" );
          }

          rename( "$dir/$file", $newFile );
        }
      }
    }
  }

  public function copyDirectory( $src, $dst ) {
    $dir = opendir( $src );
    @mkdir( $dst );

    while ( false !== ( $file = readdir( $dir ) ) ) {
      if ( ( $file != '.' ) && ( $file != '..' ) ) {
        if ( is_dir( $src . '/' . $file ) ) {
          $this->copyDirectory( $src . '/' . $file, $dst . '/' . $file );
        } else {
          copy( $src . '/' . $file, $dst . '/' . $file );
        }
      }
    }
    closedir( $dir );
  }

  /**
   * @param $file '/absolute/path/to/.gitignore'
   *
   * @return array
   */
  public function parse_git_ignore_file($file) {
    $dir = dirname($file);
    $matches = array();
    $lines = file($file);
    foreach ($lines as $line) {
      $line = trim($line);
      if ($line === '') continue;                 # empty line
      if (substr($line, 0, 1) == '#') continue;   # a comment
      if (substr($line, 0, 1) == '!') {           # negated glob
        $line = substr($line, 1);
        $files = array_diff(glob("*"), glob("$line"));
      } else {                                    # normal glob
        $files = glob("$line");
      }
      $matches = array_merge($matches, $files);
    }
    return $matches;
  }
}
