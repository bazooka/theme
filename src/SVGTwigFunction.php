<?php

namespace ThemeName;

use Fantassin\Core\WordPress\Contracts\Hooks;
use Timber\Twig_Function;
use Twig\Environment;

/**
 * Class SVGTwigFunction
 * Transform svg markup in html markup for Twig
 *
 * @package Fantassin
 */
class SVGTwigFunction implements Hooks
{

    public function hooks()
    {
        add_filter('timber/twig', [$this, 'add_to_twig']);
    }

    /**
     * @param $twig
     *
     * @return Environment
     */
    public function add_to_twig($twig)
    {
        $twig->addFunction(
            new Twig_Function(
                'get_svg',
                function (string $image_id) {
                    $res = '';
                    $image_meta = wp_get_attachment_metadata($image_id);

                    if (array_key_exists('file', $image_meta)) {
                        $upload_dir = wp_upload_dir();
                        $file = $upload_dir['baseurl'] . $image_meta['file'];
                        $res = file_get_contents($file);
                    }

                    return html_entity_decode($res);
                }
            )
        );

        return $twig;
    }
}
