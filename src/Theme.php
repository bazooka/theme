<?php

namespace ThemeName;

use Fantassin\Core\WordPress\Contracts\Hooks;
use Timber\Site;
use Timber\Theme as TimberTheme;
use Timber\Post;
use Timber\Menu;

class Theme extends TimberTheme implements Hooks {

  /**
   * Theme constructor.
   */
  public function __construct() {

    add_theme_support( 'menus' );
    register_nav_menus( [
      'primary_navigation' => 'Primary Navigation',
    ] );

    parent::__construct( 'theme_name' );
  }

  public function hooks() {
    add_filter( 'timber/context', [ $this, 'add_to_context' ] );
  }

  /**
   * @param array $context
   *
   * @return array
   */
  public function add_to_context( array $context ) : array {
    $context['lang']             = get_locale();
    $context['charset']          = get_bloginfo( 'charset' );
    $context['menu']             = new Menu( 'primary_navigation' );
    $context['site']             = new Site();

    $seopress = get_option( 'seopress_social_option_name' );

    if ( is_array( $seopress ) ) {
      $context['facebook']  = array_key_exists( 'seopress_social_accounts_facebook', $seopress ) ? $seopress['seopress_social_accounts_facebook'] : null;
      $context['twitter']   = array_key_exists( 'seopress_social_accounts_twitter', $seopress ) ? $seopress['seopress_social_accounts_twitter'] : null;
      $context['pinterest'] = array_key_exists( 'seopress_social_accounts_pinterest', $seopress ) ? $seopress['seopress_social_accounts_pinterest'] : null;
      $context['linkedin']  = array_key_exists( 'seopress_social_accounts_linkedin', $seopress ) ? $seopress['seopress_social_accounts_linkedin'] : null;
    }

    return $context;
  }
}
