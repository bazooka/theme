<?php

namespace ThemeName\Editor;

use Fantassin\Core\WordPress\Contracts\Hooks;
use ThemeName\Constants;

class Colors implements Hooks {

    public function hooks() {
        add_action( 'after_setup_theme', [ $this, 'disable_custom_colors' ] );
        add_action( 'after_setup_theme', [ $this, 'disable_custom_gradients' ] );
        add_action( 'after_setup_theme', [ $this, 'disable_editor_gradients' ] );
        add_action( 'after_setup_theme', [ $this, 'add_theme_name_colors' ] );
    }

    function disable_custom_colors() {
        add_theme_support( 'disable-custom-colors' );
    }

    function disable_custom_gradients() {
        add_theme_support( 'disable-custom-gradients' );
    }

    function disable_editor_gradients() {
        add_theme_support( 'editor-gradient-presets', [] );
    }

    // update the colors with the design system
    function add_theme_name_colors() {
        add_theme_support(
            'editor-color-palette',
            [
                [
                    'name'  => __( 'Dark Blue', Constants::TEXT_DOMAIN ),
                    'slug'  => 'dark-blue',
                    'color' => '#163467',
                ]
            ]
        );
    }
}
