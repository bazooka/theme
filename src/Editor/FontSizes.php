<?php

namespace ThemeName\Editor;

use Fantassin\Core\WordPress\Contracts\Hooks;
use ThemeName\Constants;

class FontSizes implements Hooks {

    public function hooks() {
        add_action( 'after_setup_theme', [ $this, 'set_editor_font_sizes' ] );
    }

    // update the size with the design system
    function set_editor_font_sizes() {
        add_theme_support( 'editor-font-sizes', array(
            array(
                'name' => __( 'Small', Constants::TEXT_DOMAIN ),
                'size' => 14,
                'slug' => 'small'
            ),
            array(
                'name' => __( 'Large', Constants::TEXT_DOMAIN ),
                'size' => 16,
                'slug' => 'large'
            ),
            array(
                'name' => __( 'Huge', Constants::TEXT_DOMAIN ),
                'size' => 22,
                'slug' => 'huge'
            ),
            array(
                'name' => __( 'h1', Constants::TEXT_DOMAIN ),
                'size' => 48,
                'slug' => 'h1'
            ),
            array(
                'name' => __( 'h2', Constants::TEXT_DOMAIN ),
                'size' => 33,
                'slug' => 'h2'
            ),
            array(
                'name' => __( 'h3', Constants::TEXT_DOMAIN ),
                'size' => 26,
                'slug' => 'h3'
            ),
            array(
                'name' => __( 'h4', Constants::TEXT_DOMAIN ),
                'size' => 20,
                'slug' => 'h4'
            ),
            array(
                'name' => __( 'h5', Constants::TEXT_DOMAIN ),
                'size' => 19,
                'slug' => 'h5'
            ),
            array(
                'name' => __( 'h6', Constants::TEXT_DOMAIN ),
                'size' => 13,
                'slug' => 'h6'
            )
        ) );
    }
}
