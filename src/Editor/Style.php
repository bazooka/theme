<?php

namespace ThemeName\Editor;

use Fantassin\Core\WordPress\Contracts\Hooks;
use ThemeName\ThemeAssets;

class Style implements Hooks {

    /**
     * Editor constructor
     *
     * @param ThemeAssets $theme
     */
    public function __construct( ThemeAssets $themeAssets ) {
        $this->themeAssets = $themeAssets;
    }

    public function hooks() {
        add_action( 'enqueue_block_editor_assets', [ $this, 'enqueue_block_editor_assets' ] );
        add_action( 'after_setup_theme', [ $this, 'add_supports' ] );
    }

    public function add_supports() {
        add_theme_support( 'align-wide' );
        add_theme_support( 'responsive-embeds' );
        /**
         * Support Gutenberg custom styles
         */
        add_theme_support( 'editor-styles' );
        add_theme_support( 'disable-custom-font-sizes' );
    }

    /**
     * Add theme style into Gutenberg editor
     */
    public function enqueue_block_editor_assets() {
        $this->themeAssets->set_manifest();
        wp_enqueue_style( 'theme_name-editor-style', $this->themeAssets->get_asset( 'styles/editor.css' ), array(), null, 'all' );
        wp_enqueue_script( 'theme_name-editor-script', $this->themeAssets->get_asset( 'scripts/editor.js' ), array(), array(), true );
    }
}
