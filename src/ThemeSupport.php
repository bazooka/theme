<?php

namespace ThemeName;

use Fantassin\Core\WordPress\Contracts\Hooks;

class ThemeSupport implements Hooks {

  public function hooks() {
    add_action( 'after_setup_theme', [ $this, 'add_supports' ] );
  }

  public function add_supports() {
    /**
     * Let WordPress manage the document title.
     * This theme does not use a hard-coded <title> tag in the document head, and expect WordPress to provide it for us.
     */
    add_theme_support( 'title-tag' );

    /**
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support( 'post-thumbnails' );

    /**
     * Switch default core markup for search form, comment form, and comments to output valid HTML5.
     */
    add_theme_support( 'html5' );

    /**
    * Remove the default block patterns from WordPress.
    */
    remove_theme_support( 'block-templates' );
  }
}
