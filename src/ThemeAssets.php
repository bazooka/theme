<?php

namespace ThemeName;

use Fantassin\Core\WordPress\Contracts\Hooks;
use Timber\Twig_Function;
use Twig\Environment;
use WP_Block_Type_Registry;

class ThemeAssets implements Hooks
{

    /**
     * @var string Folder where assets wil be compiled
     */
    public $buildFolder = 'build';

    /**
     * @var array Path to the manifest.json
     */
    public $manifest = [];

    public function hooks()
    {
        add_action('wp_enqueue_scripts', [$this, 'enqueue_scripts']);
        add_filter('timber/twig', [$this, 'add_to_twig']);
    }

    /**
     * @param Environment $twig
     *
     * @return Environment
     */
    public function add_to_twig(Environment $twig)
    {
        /* To load assets from view */
        $twig->addFunction(
            new Twig_Function(
                'asset',
                function ($resource) {
                    return $this->get_asset($resource);
                }
            )
        );

        return $twig;
    }

    public function enqueue_scripts()
    {
        $this->set_manifest();
        // Load default CSS files.
        wp_enqueue_style('theme_name-style', $this->get_asset('styles/main.css'), array(), null, 'all');
        // Load default JS files.
        wp_enqueue_script('theme_name-script', $this->get_asset('scripts/main.js'), array(), null, true);
        $this->enqueue_blocks_style();
        $this->enqueue_is_style();
        $this->enqueue_layout_style();
    }

    /**
     * Enqueue block styles
     */
    public function enqueue_is_style()
    {
        wp_enqueue_style('theme_name-is-style', $this->get_asset('styles/components/is-style/is-style.css'), array('theme_name-style'), null, 'all');
    }

    /**
     * Enqueue blocks specific style.
     */
    public function enqueue_blocks_style()
    {
        $block_registry = WP_Block_Type_Registry::get_instance();
        $blocks_registered = $block_registry->get_all_registered();
        foreach ($blocks_registered as $block) {
            $filename_slug = str_replace('core-', '', sanitize_title($block->name));
            $filename = $filename_slug . '.css';
            if (has_block($block->name) && file_exists($this->get_asset_path('styles/components/wp-blocks/' . $filename))) {
                wp_enqueue_style('theme_name-wp-block-' . $filename_slug . '-style', $this->get_asset('styles/components/wp-blocks/' . $filename), array('theme_name-style'), null, 'all');
            }
        }
    }

    /**
     * Enqueue layout specific style.
     */
    public function enqueue_layout_style()
    {
        global $template;
        // Get template file from template path.
        $template_name = str_replace(get_stylesheet_directory() . DIRECTORY_SEPARATOR, '', $template);
        // Replace PHP filename by CSS filename.
        $template_name = str_replace('.php', '.css', $template_name);
        $template_slug = str_replace('-css', '', sanitize_title($template_name));
        if (file_exists($this->get_asset_path('styles/layouts/' . $template_name))) {
            wp_enqueue_style('theme_name-' . $template_slug . '-style', $this->get_asset('styles/layouts/' . $template_name), array('theme_name-style'), null, 'all');
        }
    }

    /**
     * Get file URI.
     *
     * @param string $resource
     *
     * @return string
     */
    public function get_asset(string $resource): string
    {
        $resource = $this->get_resource($resource);

        return get_template_directory_uri() . DIRECTORY_SEPARATOR . apply_filters('bzk_build_folder', $this->buildFolder) . DIRECTORY_SEPARATOR . $resource;
    }

    /**
     * Get file path.
     *
     * @param string $resource
     *
     * @return string
     */
    public function get_asset_path(string $resource): string
    {
        $resource = $this->get_resource($resource);

        return get_template_directory() . DIRECTORY_SEPARATOR . apply_filters('bzk_build_folder', $this->buildFolder) . DIRECTORY_SEPARATOR . $resource;
    }

    /**
     * Get resource from manifest.
     *
     * @param string $resource
     *
     * @return string
     */
    public function get_resource(string $resource): string
    {
        $manifest = $this->get_manifest();
        if (is_array($manifest) && array_key_exists($resource, $manifest)) {
            $resource = $manifest[$resource];
        }

        return $resource;
    }

    /**
     * @return array
     */
    public function get_manifest(): array
    {
        return $this->manifest;
    }

    public function set_manifest(): ThemeAssets
    {
        $path = get_theme_file_path(apply_filters('bzk_build_folder', $this->buildFolder) . DIRECTORY_SEPARATOR . 'manifest.json');
        if (file_exists($path)) {
            $manifest = file_get_contents($path);
            $this->manifest = json_decode($manifest, true);
        }

        return $this;
    }
}
