<?php

use Timber\Post;
use Timber\PostQuery;
use Timber\Timber;

/**
 * The template for displaying Archive page.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$context = Timber::context();
$context['posts'] = new PostQuery();
$context['categories'] = Timber::get_terms('category');
Timber::render( array( 'home.twig' ), $context );
