<?php

use Fantassin\Core\WordPress\Theme\ThemeKernel;
use ThemeName\Constants;
use Timber\Timber;

$autoload = __DIR__  . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

if ( ! file_exists( $autoload ) ) {
    add_action( 'admin_notices', function () {
        echo '<div class="error"><p>Make sure you already download PHP dependencies with <code>composer install</code></p></div>';
    } );

    return;
}

require_once $autoload;

if ( ! class_exists( '\\' . Timber::class )) {
    add_action(
        'admin_notices',
        function () {
            echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url(
                    admin_url('plugins.php#timber')
                ) . '">' . esc_url(admin_url('plugins.php')) . '</a></p></div>';
        }
    );

    return;
}

if ( ! defined('TIMBER_LOC')) {
    define('TIMBER_LOC', __DIR__);
}

/**
 * Sets the directories (inside your theme) to find .twig files
 */
Timber::$dirname = array('views');
Timber::$cache   = ! WP_DEBUG;

/**
 * By default, Timber does NOT autoescape values. Want to enable Twig's autoescape?
 * No prob! Just set this value to true
 */
Timber::$autoescape = false;

class ThemeName extends ThemeKernel
{
    public function getTextDomain(): string
    {
        return 'theme-name';
    }
}

$themeName = new ThemeName(wp_get_environment_type(), WP_DEBUG);
$themeName->load();

