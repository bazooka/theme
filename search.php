<?php

use ThemeName\Constants;
use Timber\PostQuery;
use Timber\Timber;

/**
 * Search results page
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$templates = array( 'search.twig', 'archive.twig', 'front-page.twig' );

$context          = Timber::get_context();
$context['title'] = 'Search results for ' . get_search_query();
$context['posts'] = new PostQuery();
$context['search_query'] = get_search_query();

$number = $context['posts']->found_posts;

if ($number > 0) {
    $context['result_number'] = $number . ' ' . __( 'results', Constants::TEXT_DOMAIN );
} else {
    $context['result_number'] = __( 'No results', Constants::TEXT_DOMAIN );
}

Timber::render( $templates, $context );
